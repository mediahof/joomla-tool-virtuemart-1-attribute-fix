<?php
/**
 * @author     mediahof, Kiel-Germany
 * @link       http://www.mediahof.de
 * @copyright  Copyright (C) 2014 mediahof. All rights reserved.
 * @license    GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */

define('_JEXEC', 1);

define('DS', DIRECTORY_SEPARATOR);

if (file_exists(dirname(__FILE__) . '/defines.php')) {
    include_once dirname(__FILE__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__FILE__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_BASE . '/includes/framework.php';

JFactory::getApplication('site');

new VirtueMartFixProductAttributes;

final class VirtueMartFixProductAttributes
{

    private $db = null;

    private $products = array();

    public function __construct()
    {
        $this->db = JFactory::getDbo();

        $this->checkTables();

        $this->loadProducts();

        $this->calcAttributePrices();

        $this->fixAttributePrices();

        $this->setAttributePrices();

        echo '<pre>' . print_r($this->products, true) . '</pre>';
    }

    private function checkTables()
    {
        $tables = $this->db->getTableList();
        $prefix = $this->db->getPrefix();

        if (!array_search($prefix . 'vm_product', $tables) || !array_search($prefix . 'vm_product_price', $tables)) {
            exit('no VirtueMart 1.x tables found');
        }
    }

    private function loadProducts()
    {
        $query = $this->db->getQuery(true)
            ->select('p.product_id')
            ->select('pr.product_price')
            ->select('p.attribute')
            ->from('#__vm_product AS p')
            ->join('INNER', '#__vm_product_price AS pr ON (p.product_id = pr.product_id)')
            ->where('p.attribute LIKE ' . $this->db->quote('%[=%]%'));

        $this->db->setQuery($query);

        $this->products = $this->db->loadObjectList();

        if (empty($this->products)) {
            exit('no relevant products found');
        }
    }

    private function calcAttributePrices()
    {
        foreach ($this->products as &$product) {
            preg_match_all('#=([0-9.]{1,10})#', $product->attribute, $product->variants);
            if (isset($product->variants[1])) {
                foreach ($product->variants[1] as &$variant) {
                    $variant = $variant - $product->product_price;
                    if ($variant > 0) {
                        $variant = '+' . $variant;
                    }
                }
            }
        }
    }

    private function fixAttributePrices()
    {
        foreach ($this->products as &$product) {
            $search = $replace = array();

            foreach ($product->variants[0] as $key => $variant) {
                $search[] = '[' . $variant . ']';
                $replace[] = '[' . $product->variants[1][$key] . ']';

                $product->attribute = JString::str_ireplace($search, $replace, $product->attribute);
                unset($product->variants, $product->product_price);
            }
        }
    }

    private function setAttributePrices()
    {
        foreach ($this->products as &$product) {
            $this->db->updateObject('#__vm_product', $product, 'product_id');
        }
    }
}